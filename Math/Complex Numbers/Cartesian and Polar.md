# Cartesian
![cartesian](cartesian.png)

$Z=a+jb$
$a=r \cdot cos\varphi$
$b=r\cdot sin \varphi$

# Polar
![polar](polar.png)

$Z=r^\angle \phi$
$r=\sqrt{a^2+b^2}$
$\varphi=arctan(\frac{b}{a})$ For Quadrant 1&4
$\varphi=180+arctan(\frac{b}{a})$ For Quadrant 2&3
