A comparator can either be a specific IC or a regular [[OpAmp]]. It constantly compares both its inputs and gives out the positive or negative supply depending on which input has the higher voltage.

Is the non inverting input higher than the inverting input, then the positive rail is output. Is the inverting input higher, the negative rail.