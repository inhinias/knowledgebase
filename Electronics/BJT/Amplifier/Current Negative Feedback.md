The [[Transistor Amplifier]] has a resistor at the emitter. This resistor acts as a negative feedback loop for the Transistor.

The transistor T1's amplification is dependant on the base-emitter voltage. As more current flows through $R_E$ the voltage drop over it rises.
This causes $U_{BE}$ to be lowered.