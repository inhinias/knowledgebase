$U_{eff} = Û \cdot \sqrt{ \frac{1}{2} - \frac{t_0}{T} + \frac{1}{4\pi} \cdot sin(4\pi\frac{t0}{T}) }$
The idea of phase angle control is to control mostly ohmic loads by cutting off parts of the sine wave running across the load. Thus reducing the power over said load.

By cutting off from the start of the sine wave, [[DC-Inductor|inductive loads]] are possible. (Mitigation of the inductor causing voltage spikes) 

By cutting off from the end of the sine wave, [[DC-Capacitor|capacitive loads]] are possible. (Mitigation of the capacitor causing current spikes)


![[Phase-Angle.png|400]]

The working principle is to detect zero crossings in an ac sine wave and use a certain delay to these crossing to ignite a [[Triac]]. This ignition causes the circuit to be closed not from the start of the sine wave bit on a certain point on the wave its self. The ignition mostly happens through a [[Diac]] at the triac's gate delayed via a [[DC-Capacitor|RC-Time-Delay]] 

![[P-A-Circuit.png|600]]