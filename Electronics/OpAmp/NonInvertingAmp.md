![[noninvertingAmp.png|500]]

This amplifier does not invert the input Signal and amplifies it. It is very similar to the [[Inverting Amp]] except that the [[OpAmp|OpAmp's]] inputs are swapped

Note: Due to the 1+ in the Formula, this amp can NOT have a amplification lover than 1. Thus it can only amplify and not dampen.

$V = 1+\frac{R_2}{R_1}$
$U_{out} = U_{in} \cdot (1+\frac{R_2}{R_1}) = U_{in} \cdot V$