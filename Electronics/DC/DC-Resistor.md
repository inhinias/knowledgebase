The simplest of all components to calculate is a resistor in DC. It strictly follows ohms law
$U=R \cdot I$

$P = R \cdot I$