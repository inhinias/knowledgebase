There are two basic rules to an Op-Amp that allow one to analyze pretty much any circuit containing them.

### No current flow into the Op-Amps inputs
### The Op-Amp tries to keep both its inputs at the same voltage

Note: There is still a [[Bias Compensation|Bias Current]] flowing into the inputs. Its very very tiny thou so its usually ignored.
Note: The Op-Amp can't technically keep its inputs at exactly the same voltage. This does not influence any calculations thou.