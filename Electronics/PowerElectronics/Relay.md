The magnetic field of an [[DC-Inductor|inductor]] moves a contactor switch. Thus opening and closing a isolated circuit by powering a coil. Cause the switching element uses a coil, a relay need a [[DC-Inductor|flyback]] [[Diode]] in order to avoid voltage spikes when switching the coil off.
- Isolated input and output
- No power loss across the output
- Wear on the contactors especially when the output stays powered
- Slow switching times due to it being mechanical
- Considerable power draw on the input side
- Come in two flavors [[MechanicalSwitches|NC or NO]]