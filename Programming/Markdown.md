| What                    | How				                 | How it's supposed to look       |
|-------------------------|----------------------------------|---------------------------------|
| Blockquote              | \> This is a block quote.        | > This is a block quote.        |
| Code                    | \`var f = new function()\`       | `var f = new function()`          |
| Escape Character        | \\\<\\\*\\\*\\\>                 | \<\*\*\>                        |
| Heading 1               | \# This is H1                    | # This is H1                    |
| Heading 2               | \#\# This is H2                  | ## This is H2                   |
| Italics / Emphasis      | \_emphasized_                    | _emphasized_                    |
| Link                    | \[Google](http://www.google.com) | [Google](http://www.google.com) |
| Link (Labeled with URL) | \<http://www.google.com>         | <http://www.google.com>         |
| Numbered (Ordered) List | \1. This is number one.          | 1. This is number one.          |
| Unordered List          | \- This is a list item.          | - This is a list item.          |
| Table Formatting        | Cell 1\|Cell 2\|Cell 3           | Cell 1\|Cell 2\|Cell 3          |