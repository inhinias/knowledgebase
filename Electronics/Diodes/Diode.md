Diodes are an electrical component that will only let current flow in one direction. (Forward Bias)

![Diode IV Diagramm](Diode-IV-Curve.png)

In its forward direction a regular diode has a typical voltage drop of 0.7V. This drop is the same for all regular diodes. [[Schottky Diode|Schottky Diodes]], and [[ZenerDiode|ZenerDiodes]] both have varying voltage drops.
A regular diode cannot resist an infinite voltage in reverse. The maximum reverse voltage is its breakdown voltage. Cross it and the PN-Layer in the Diode is dead. Also note that diode have a tiny reverse leakage current as seen in the graph.

Temperature coefficient: -2mV/K

At around 0.7V a diode becomes conductive really fast. A tiny increase in voltage causes a huge increase in current that can flow across it.
