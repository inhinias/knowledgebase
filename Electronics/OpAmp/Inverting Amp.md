![[invertingAmp.png|500]]

This amplifier inverts the input Signal and amplifies it. It is very similar to the [[NonInvertingAmp]] except that the [[OpAmp|OpAmp's]] inputs are swapped

$V = \frac{R_2}{R_1} = \frac{U_{in}}{U_{out}}$
$U_{out} = U_{in} \cdot V$