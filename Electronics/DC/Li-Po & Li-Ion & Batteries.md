### LiPo
![[LiPo.jpeg|400]]
Typically in a square cell pouch form with a wide range of capacities and sizes.

Nominal cell voltage: 3.7 V
Charging voltage: 4.2V
Storage Voltage: 3.8V

### Li-Ion
![[li-ion.jpeg|400]]
Always a round cell similar to regular batteries thou at a larger size. Typically sizes are 18650 (1.5–3.5 Ah), 2170 (3-5 Ah) and 4680 (~9 Ah)

Nominal cell voltage: 3.7 V
Charging voltage: 4.2V
Storage Voltage: 3.8V

### LiFePo
Has a lower energy density than other lithium based batteries but at the advantage that it doesn't tend to combust even under mechanical breakage. It has a lower nominal voltage of 3.2-3.3V. 

### Lead Acid
The car battery. Normally at 12V sometimes at 6V for smaller versions. It is able to push huge amounts of current at cold temperature (Labeled as CCA, cold cranking amps) this is cause a starter motor can easily use several hundred amps when starting the motor.

### Charging
LiPo and Li-Ion batteries can be charged the same way. Starting with a Constant Current of typically 1C meaning at 1x its capacity (2Ah battery charges at 2A) if the battery voltage reaches 4.2V it is no longer charged with a constant current but with a constant voltage.