$y=mx+q$
$m$ Steepness of the function
$q$ Zero crossing point (x=0, y=q)

The angle the linear curve has: $\alpha = tan^{-1}(m)$