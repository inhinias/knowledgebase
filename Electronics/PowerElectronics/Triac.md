"Triode for alternating current"
The triac is similar to a [[Thyristor]] with the main difference being, that a triac can conduct current in both directions instead of just one like the thyristor.
![[Triac.png|200]]
In its initial state, the triac has a high impedance between its two conductors. Due to a current pulse at the gate, the triac gets "ignited". Therefore changing to low impedance between its conductors. It stays in this conductive state until the current through it falls below its holding current. At this stage the triac needs to be reignited in order to become conductive again.