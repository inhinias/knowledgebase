LED's are basically a [[Diode]] that emits light. Therefore they can't be driven similarly to a light bulb.
Led's should run with a constant current but this only becomes a necessity when there are a lot of them or they are high power.

Otherwise use a resistor in series to limit the current. $R_s = \frac{U_{VCC} - U_{LED}}{I_{LED}}$
The LED's voltage is dependant on its color. Here are some rough values:

| Color  | Voltage  |
|--------|----------|
| IR     | 1.5V     |
| Red    | 1.6-2.1V |
| Orange | 1.8V     |
| Green  | 2.1V     |
| Yellow | 2.1      |
| Blue   | 2.9V     |
| White  | 4.0V     |

Note: LED's can't hold a very high reverse voltage ~5V!