Short for Insulated Gate Bipolar Transistor
It's a mix between a [[FET Overview|FET]] and a [[BJT]]. Wherein the FET controls the base of the BJT. Thus creating an insulated high current semiconductor.

![[IGBT.png|600]]