# Permissions
Show all permissions in the current dir
> ls -la

| Owner | Group | Other |
|-------|-------|-------|
| RWX   | R-X   | R-X   |

### chmod
> chmod 777 someFile.f

The first number is for the file owner, the second for the group and the third for others.

| Value | Permission |
|-------|------------|
| 0     | None       |
| 1     | x          |
| 2     | w          |
| 3     | w+x        |
| 4     | r          |
| 5     | r+x        |
| 6     | r+w        |
| 7     | r+w+x      |

Recursive on all file in a directory
> chmod -R
