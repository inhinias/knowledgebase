![BJT-Symbols](BJT-Silicon&Symbols.png)

### Flavours
NPN (Never Points In) and PNP (Points In Permanently).

### Pin Mapping
- Base
	- Used to control the BJT, always to the side of the Collector-Emitter-Path
- Collector
	- The complement to the emitter
- Emitter
	- Is always the one with the arrow. The arrow indicates the current flow for controlling the Transistor.
		- NPN: $U_B > U_E$ current flows from base to emitter.
		- PNP: $U_B < U_E$ current flows from emitter to base.

### Using
The voltage $U_{BE}$ needs to be below $1V$ cause the Base-Emitter-Path is basically a [[Diode]]. (The actual value is in the datasheet under $U_{BE Max}$)
Thus a current limiting resistor needs to be placed at the base. Its value is: $R_B = \frac{UB - U_{BE}}{I_B} = \frac{(UB - U_{BE}) \cdot B \cdot ü}{I_C}$
B (sometimes $h_{FE}$) is given by the datasheet as current gain. ü is the overdrive factor its normally between 2 and 5. This factor is needed to saturate the transistor when operating it as a switch.

There is a voltage drop across the Collector-Emitter-Path and across the Base-Emitter-Path. This is the main reason why BJT's are not used to switch high currents as these voltage drops cause a big power loss.


### Notes
- This current gain is sometimes dependant on a specific transistor type. So pay care to extra lettering when reading out the current gain.
- The current gain drops off with a higher collector current.
- They need considerable amount of current at the base.