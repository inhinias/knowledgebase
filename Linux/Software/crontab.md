# Cron
Cron is a service running in the background that can periodically run user defined commands.

The config is in *[[etc|/etc]]/crontab*

Shortest interval: 60s
Won't try a failed command again till the next time the command is run.

### List all active jobs
> crontab -l
>  crontab -u username -l

### Edit cron jobs
> crontab -e

![cronConfig](crontab-syntax.png)

For example: run *root/backup.sh* every Friday at 17:37
>37 17 * * 5 root/backup.sh

-   **Asterisk \*** Signify all possible values in a field
-   **Comma ,** List multiple values. E.g. **1,5** in the **Day of the week** field will execute the task every Monday and Friday.
-   **Hyphen -** Define a range of values. E.g. From July to August, write *7-8* in the *Month field 
-   **Separator /** If you want to make a script run every twelve hours, write */12* in the *Hour* field.
-   **Last L** Can be used in the day-of-month and day-of-week fields. Writing *2L* in the *day-of-week* field executes the command on the last tuesday of a month.
-   **Weekday W** Run on the closest weekday of the give time point.
- **MAILTO="someone@domain.tld"** Will mail the output of a command to the specified address.