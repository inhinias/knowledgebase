![[summingAmp.png|400]]

A summing amp mixes all the input signals together and amplifies them individually based on the resistor ($R_1...R_n$) in line with only that signal.
The resistor $R_F$ provides feedback for the [[OpAmp]].

Note: The Summing amp is inverting!

$-U_{out} = \frac{R_F}{R_1} \cdot U_1 + \frac{R_F}{R_2} \cdot U_2 + \frac{R_F}{R_n} \cdot U_n$