$K_n = K_0 \cdot (1+\frac{p}{100})^n$

$K_n$ Final amount
$K_0$ Starting amount
$p$ Percentage value in a whole number eg. 0-100
$n$ time duration