A diode that can basically have a custom "breakdown" voltage drop across it from factory. It still has the regular 0.7V drop across it in forward bias.

### Usage
#### Voltage Reference
A basic [[Voltage References|Voltage Reference]] is made by running a zener diode in reverse and adding a resistor infront of it to limit the current running through it. This current should be ~10x bigger than any current flowing off to anything using this reference. Consider the diode and resistor unloaded that way. This voltage reference is not good with temperature differences and zener diodes are not the most accurate reference. (~10-100mV)

#### Overvoltage Protection
As zener diodes conduct over a certain voltage they will conduct off any overvoltage for a short amount of time (Voltage Peaks). This is commonly known as a TVS diode. It is also possible to have to connected against each other so voltage spikes are suppressed on the power and GND rail.

![[TVS.png|400]]