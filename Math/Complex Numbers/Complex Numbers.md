# Definition
$j^2=-1$
$Z_1 = a+jb Z_2 = c+jd$

## Addition/Subtraction
$Z = Z_1+Z_2 = a+c+jb+jd$
$Z = Z_1-Z_2 = a-c-jb-jd$

## Multiplication
$Z = Z_1 \cdot Z_2 = (a+jb) \cdot (c+jd) = ac+jad+jbc+J^2bd = ac-bd +j(ad+bc)$

## Division
Never use a complex number in the divisor. Convert it to a real number!

$Z= \frac{Z_1}{Z_2}$
$Z= \frac{a+jb}{c+jd}=\frac{(a+jb)\cdot(c-jd)}{(c+jd)\cdot(c-jd)}$
$Z=\frac{ac+bd}{c^2+d^2} +j\frac{bc-ad}{c^2+d^2}$
