![Amplifier](amp.png)

### Components
- C1: DC-Blocking Capacitor for AC-Only input. See [[DC-Capacitor]]
- R1 & R2: Voltage divider for preloading the BJT Base
- RC: Collector resistor
- RE1, RE2 & CE Current feedback / Amplification factor resistor (RE1 for AC, RE1+RE2 for DC)
- C2: DC-Blocking Capacitor for AC-Only output
- RL: Load resistor for power adapting of the output
- T1: [[BJT|NPN Transistor]]

### DC-Dimensioning
$R_C = R_L = r_a = \frac{U_{RC}}{I_C}$

$U_{RE} =$ ~10-20% of U_B (1-2V)

$U_{RC} = \frac{U_B - U_{RE} - U_{BE}}{2}$

Drift amplification: $v_D = 2 ... 10 = \frac{R_C}{R_E}$

$V_{AC} = \frac{R_C||R_L}{R_{E1}}$

$R_E = R_{E1} + R_{E2}$

$I_E = \frac{U_{RE}}{R_E} = I_C + I_B = I_C + ( \frac{I_C}{B} )$

$I_C = I_B \cdot B$

$I_{R2} = 9 * I_B$

$I_{R1} = 10 \cdot I_B$

$R_2 = \frac{U_{RE} + U_{BE}}{I_{R2}}$

$R_1 = \frac{U_B - U_{RE} - U_{BE}}{I_{R2} + I_B}$