# Session Initiation Protocol
Mainly used for IP-Phones as their primary communication protocol. A Device first registers with a PBX (Private Branch Exchange). The PBX handles addressing and NAT between phones.

## Registration
![register](SIP-registration-flow.png)
Note: A phone registers first without auth and then with. So the first request might be denied.

## Call building
![register](SIP-B2BUA-call-flow.png)

The compete exchange happens over the PBX. It handles NAT and associating phone numbers to a phones IP-address.
The media (audio and/or video) can, but normally does not, flow through the PBX. It can be done to overcome NAT issues. Media is transported via [[RTP]]

## Status Codes
The status codes are similar but not equivalent to the one of [[HTTP&HTTPS|HTTP]].